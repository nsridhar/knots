filename=PNAS-template-main
pdf: dvi
		dvipdfmx ${filename}.dvi

pdf-print: ps
		ps2pdf -dColorConversionStrategy=/LeaveColorUnchanged -dPDFSETTINGS=/printer ${filename}.ps

ps:	dvi
		dvips -t letter ${filename}.dvi

dvi:
		latex ${filename}
			bibtex ${filename}||true
				latex ${filename}
					latex ${filename}
clean:
		rm -f ${filename}.{ps,pdf,log,aux,out,dvi,bbl,blg}


