\documentclass[9pt,twocolumn,twoside,lineno]{pnas-new}
% Use the lineno option to display guide line numbers if required.
\usepackage{setspace}
%\usepackage{pdfpages}
\usepackage{graphics}
\doublespacing
\templatetype
%{pnasresearcharticle} % Choose template 
%{pnasresearcharticle} = Template for a two-column research article
{pnasmathematics} %= Template for a one-column mathematics article
%{pnasinvited} %= Template for a PNAS invited submission

\title{Folding Pathways for Proteins with Deep Trefoil and Gordian Knots}

% Use letters for affiliations, numbers to show equal authorship (if applicable) and to indicate the corresponding author
\author[a,b,1]{Sridhar Neelamraju}
\author[a,2]{Shachi Gosavi} 
\author[b]{David Wales}

\affil[a]{Simons Centre for the Study of Living Machines, National Centre for Biological Sciences, GKVK Campus, Bengaluru, 560065}
\affil[b]{University Chemical Laboratories, Lensfield Road, Cambridge, CB2 1EW }

% Please give the surname of the lead author for the running footer
\leadauthor{Neelamraju} 

% Please add here a significance statement to explain the relevance of your work
\significancestatement{Authors must submit a 120-word maximum statement about the significance of their research paper written at a level understandable to an undergraduate educated scientist outside their field of speciality. The primary goal of the Significance Statement is to explain the relevance of the work in broad context to a broad readership. The Significance Statement appears in the paper itself and is required for all research papers.}

% Please include corresponding author, author contribution and author declaration information
\authorcontributions{Please provide details of author contributions here.}
\authordeclaration{Please declare any conflict of interest here.}
\equalauthors{\textsuperscript{1}A.O.(Author One) and A.T. (Author Two) contributed equally to this work (remove if not applicable).}
\correspondingauthor{\textsuperscript{2}To whom correspondence should be addressed. E-mail: author.two\@email.com}

% Keywords are not mandatory, but authors are strongly encouraged to provide them. If provided, please include two to five keywords, separated by the pipe symbol, e.g:
\keywords{Knotted proteins $|$ Energy landscapes $|$ Folding pathways $|$ ...} 

\begin{abstract}
%Please provide an abstract of no more than 250 words in a single paragraph. Abstracts should explain to the general reader the major contributions of the article. References in the abstract must be cited in full within the abstract itself and cited in the text.
It is still not obvious how, during folding, a substantial part of a protein can form a complex knot deep inside the backbone in a thermodynamically favoured, downhill manner. To understand possible mechanisms of knot formation, we construct the energy landscape of two proteins with a trefoil knot and one protein with a Gordian knot in the backbone. We employ geometry optimisation to find connected paths between distant conformations on the energy landscape. The landscape is described as a kinetic transition network of local minima and transition states that connect them, visualised using disconnectivity graphs. From this network, we identify topological traps and multiple kinetically relevant pathways that can lead to a native knotted topology. The methodology can be generalised to derive multiple pathways for folding any entangled protein with a complex topology. In particular, we find that the slip-knotted intermediate is the most populated kinetic trap for both deep and shallow trefoil knots. The most favourable path from a slip-knot to a deep trefoil knot comprises a combination of a mobile loop flipping over the N-terminus followed by the N-terminus entering the mobile loop through a slip-knotting motion. A jammed trefoil knotted topological trap exists for the Gordian knot.
\end{abstract}

\dates{This manuscript was compiled on \today}
\doi{\url{www.pnas.org/cgi/doi/10.1073/pnas.XXXXXXXXXX}}

\begin{document}

\maketitle
\thispagestyle{firststyle}
\ifthenelse{\boolean{shortarticle}}{\ifthenelse{\boolean{singlecolumn}}{\abscontentformatted}{\abscontent}}{}

% If your first paragraph (i.e. with the \dropcap) contains a list environment (quote, quotation, theorem, definition, enumerate, itemize...), the line after the list may have some extra indentation. If this is the case, add \parshape=0 to the end of the list environment.
\dropcap{I}t is quite surprising that some naturally occurring proteins spontaneously fold into a complex knotted topology \cite{Mallam08a,Faisca15a,Mallam08b,Jackson17a,Virnau06a}. Over 600 entries of proteins with knots exist in the protein data bank  \cite{Tumanski18a}. The most complex knotted topology is that of a microbial enzyme that facilitates the breakdown of pollutants such as pesticides and herbicides \cite{Bolinger10a,Schmidberger08a}. The smallest knotted protein comprising only 92 amino acids was found in a hyperthermophilic archea, \textit{Methanocaldococcus jannaschi}, found in the deep sea capable of surviving at 85 $^\circ$C \cite{Thiruselvam17a}. Knotted topologies are conserved across families, indicating a possible functional advantage \cite{Sulkowska12a}. It has been argued that knots might be crucial for avoiding degradation and enhancing thermodynamic stability \cite{Jackson17a}, and that they may help hold together different protein domains \cite{Virnau06a} and provide cellular stability, or stabilise trans-membrane channels\cite{Sulkowska12a,Faisca15a}. It is also possible to reversibly fold a few knotted proteins through chemical and thermal denaturation without the help of chaperones \cite{Mallam08b,King10a, Noel13a,Wang15a}. 

Multiple experimental studies have noted  populated intermediates for various knotted proteins \cite{Andersson09a,Chuang19a,Mallam05a,Mallam06a,Mallam08a,Mallam08b,Wang15a,Wang16a,Lim03a,Chuang19a}. In contrast to single-domain natural proteins that fold in a funnelled manner to their native biologically functional state, knotted proteins appear to support many traps and a frustrated energy landscape\cite{Wallin07a, Prentiss10a}. Thus, characterising the underlying nature and properties of the potential energy surface for a deeply knotted protein is an interesting challenge. 
Structure-based models (SBMs) encode the native structure of the protein, usually leading to funnelled energy landscapes. These models isolate the effects of topology by removing energetic trapping due to non-native interactions \cite{Levy04a,Neelamraju19a,Neelamraju18a}. A diverse set of natural, single-domain, globular proteins are accurately described by SBMs  \cite{Clementi08a}. Folding behaviour of various types of knotted proteins \cite{Noel13a,Noel10a,Sulkowska12a,Sulkowska09a} were also described with SBMs. In combination with molecular dynamics studies \cite{Xu18a, Wu15a}, folding mechanisms of trefoil\cite{Faisca15a,Skrbic12a,Sulkowska09a,Sulkowska12a,Noel10a,Noel13a},  Gordian \cite{Zhao18a} and Stevedore type knots\cite{Bolinger10a, Wang16a} were studied. However, molecular dynamics simulations have been less successful is obtaining a detailed description of the energy landscapes for proteins with deep trefoil knots \cite{Wallin07a}. This situation is a consequence of the inability to consistently fold and unfold to deeply knotted native topology. In the present work, we present a general strategy for describing the energy landscape of proteins that fold into complex entangled topologies such as slip-knots, lassos, links and knots. A kinetic transition network \cite{Wales03a,Becker97a} consists of local minima and intervening transition states is derived with discrete path sampling. Kinetic transition networks are a coarse-grained representation of the energy landscape that preserve the high dimensionality of the potential energy surface\cite{DPS1}. From this network, we extract multiple distinct pathways that lead to a native topology. We also identify possible topological traps and the magnitude of topological barriers separating various local minima and transition states on the potential energy landscape. The network is then visualised as a disconnectivity graph \cite{Becker97a,Wales98a}.
\section*{Results}
\subsection*{Energy landscape of MJ0366: The simplest trefoil knot}
Multiple theoretical \cite{Sulkowska12a,Sulkowska09a,Noel10a,Noel13a,QCI2,Silvio13a,Faisca15a,Wu15a} and experimental \cite{Jackson17a,Mallam08b,Mallam08a,Chuang19a,Wang15a} studies have been carried out on proteins that fold into trefoil knots. We first apply our method to a well-studied model trefoil knot, MJ0366, and then extend it to a deep trefoil knot formed by another well-studied protein, a YibK methyltransferase and a Gordian knot formed by a deubiquitinating enzyme, UCH-L1. 

The disconnectivity graph for MJ0366 is depicted in Figure \ref{fig:MJ0366}. Two major sub-basins appear in the low-energy part of the landscape. The bottom of the first sub-basin (corresponding to structure A1) belongs to a state where the first threading event proceeds from the C-terminus, and the N-terminal region is in a jammed slip-knotted state. The jammed slip-knot structure has not been reported as a kinetic trap for this protein before. However, a similar jammed structure was reported for a slipknot formed by a human ribonuclease from pulling molecular dynamics simulations \cite{Sulkowska09a}. At low temperatures, the jammed slipknot conformation A1 is a topological trap. Minima belonging to this basin compete in thermodynamic stability with other structures where a native-like knot has already been formed. The bottom of the second sub-basin (corresponding to structure A2), results from the the N-terminal forming native-like contacts without actually going through the loop motif formed by the helices $\alpha$1 and $\alpha$2. The structure corresponding to the A2 basin was reported as a trap by Sulkowska et al. \cite{Sulkowska12a,Noel10a} for MJ0366 with a potential identical to ours.

From previous molecular dynamics studies, it has been hard to quantify at what point on the energy landscape the process of knotting begins. To this end, we classify each local minimum and transition state in the database as knotted or unknotted \cite{Tubiana18a}. The knotted local minima are depicted on the disconnectivity graph in  Figure \ref{fig:MJ0366} as green nodes and the unknotted ones as black nodes. All local minima belonging to the A1 and A2 sub-basins remain unknotted. The protein thus needs to overcome a significant topological barrier of  15$\varepsilon$ from the bottom of the A1 basin, and a similar 15$\varepsilon$ from the bottom of the A2 basin resulting in the so-called ``backtracking'' mechanism\cite{Noel10a}. In each case, on average 15 contacts need to be broken and reformed to reach the native-like knot. The correct form of knotting begins  around 42$\varepsilon$ above the native state. Under an energy of 33$\varepsilon$, only knotted structures appear on the graph except for the ones corresponding to the A1 sub-basin. High-energy knotted states also appear in the graph. These are examples of knots forming in a non-native like topology (non-specific knots). Examples of some high energy knotted states are provided in the Supplementary Information. 

\subsubsection*{Distinct pathways to the native trefoil knotted state}

% \begin{figure}
%     \centering
%     \includegraphics[width=0.4\textwidth]{2efv_kdpaths_fig.eps}
%     \caption{S }
% \end{figure}

A kinetic transition network may support many distinct pathways between any two local minima. In Figure \ref{fig:kdpaths_2efv}, we show the most kinetically relevant distinct paths \cite{Sharpe19a} found on the landscape connecting the unknotted topological traps A1 and A2 to the knotted native-state N. In both these structures, a native-like pre-ordered loop, comprising the $\alpha$1-$\alpha$2 helices, and the C-terminal ($\alpha$4) have attained a native-like structure, and the N-terminus ($\beta$2) needs to go through the pre-ordered loop to attain the native trefoil structure.

The distinct paths  (Figure \ref{fig:kdpaths_2efv}) are possible routes on the kinetic transition network that are most likely to guide the protein to its native knotted configuration. Distinct paths were identified on the kinetic transition network (Figure \ref{fig:MJ0366}) with a scalable path deviation algorithm \cite{Sharpe19a}. Three physically distinct pathways emerge, coloured red, blue and green respectively. The stationary points along a representative pathway for Figure \ref{fig:kdpaths_2efv}a are depicted in Figure \ref{fig:kdpaths_2efv_fig} and a video with three representative paths connecting A1 to N and A2 to N is also included in the Supplementary Information. 

Formation of a native-like pre-ordered loop comprising $\alpha$1-$\alpha$2 helices provides a loop for one of the termini to pass through first. The other terminus then passes through the same pre-ordered loop to reach the native state. We predict that the preferred pathway is where the helical C-terminus enters an already ordered loop through a threading motion followed by the entry of the N-terminal through a slip-knotting motion. This path is in red in Figure \ref{fig:kdpaths_2efv}a and b. Curiously, we do not observe the threading mechanism for the N-terminal. The C-terminus comprises an $\alpha$-helix ($\alpha$4). Structure Based Models ensure rigidity of helices and this motif therefore behaves like a collective block, always resulting in a threading motion. The N-terminus, on the other hand, is a $\beta$-strand which is more flexible. The slip-knotting mechanism is overwhelmingly favoured as the lowest-energy pathway to the native state in the case of the N-terminal. This is also intuitive as this way fewer contacts need to be broken and reformed until the native-state is obtained ensuring a low-energy pathway. The threading mechanism, in contrast, would require multiple contacts to break in one step for one of the terminal residues to enter the pre-ordered loop, which would lead to a higher potential energy barrier. This pre-ordered native loop is stable up to around 150$\varepsilon$ above the native state in the distinct paths found indicating the thermodynamic stability of this particular motif. 

From molecular dynamics simulations at folding temperature,  it was argued that a native-biased landscape was sufficient to fold complex topologies, and that knotting via threading a native-like loop in a pre-ordered intermediate was a mechanism generalisable to all trefoil knotted proteins \cite{Noel10a}. With a kinetic transition network, we can to identify topological traps, the topological barrier to the pre-ordered native-like loop, identify where on the potential energy landscape the process of knotting begins, and identify distinct kinetically relevant paths that lead to the native-knotted topology. We also found a new topological trap for this protein in a jammed slip-knot structure. It has since been suggested that proteins with deep trefoil knots follow a very different folding mechanism from the model trefoil knot studied above\cite{Wallin07a} and are quite difficult to fold with a native-biased potential like ours. We investigate the energy landscape of a deep trefoil knot in the next section.
\subsection*{Energy landscape of a deep trefoil knot: YibK}


YibK is a 160 residue protein that contains a deep knot in its native structure \cite{Lim03a}. Molecular dynamics studies on this protein found an increased success rate for knot formation by biasing a C$\alpha$-only model with additional specific, attractive non-native interactions. The folding success rate was very poor for the C$\alpha$ only model and no knot formation occurred\cite{Wallin07a}. It was also reported that the unfolded and native states were kinetically connected but molecular dynamics simulations suggested an exceedingly low probability of observing the knotted state\cite{Wallin07a}. With the discrete path sampling methodology \cite{DPS1}, with end points connected with a quasi-continuous interpolation scheme  \cite{Wales12a,QCI2}, we are able to characterise paths connecting knotted and unknotted states without having to bias the original C$\alpha$-model while largely avoiding the problem of chain crossings.
	
Since we are interested in the mechanism for both deep and shallow knot formation, we truncate YibK in the following manner: We define a shallow knot as having up to 5 residues on either side of the knotted region in the native structure, and a deep knot as one that has 20 residues or more on either side of the knotted region in the native structure. The knotted region, identified from the KMT reduction algorithm \cite{Koniaris91a}, has 43 residues (numbers 77 to 119 in PDBID:1MXI). Structure Based Models were derived\cite{Neelamraju19a} from the native structure truncated to a shallow knot (five residues on either side of the knotted region; 53 in total) and a deep knot (20 residues on either side of the knotted region; 93 residues in total). We note the contact map includes native interactions in residues that are not a part of the knotted region as well (see Supplementary Information for details). 

The energy landscapes derived for the shallow and deep knot in YibK  are showed in Figure \ref{fig:HI0766}. The deep trefoil knot is more frustrated than the shallow knot. The landscape of the shallow knot is well funneled and very little topological trapping exists. Analogous to MJ0366, the most significant topological trap for the deep trefoil knot is also a jammed slip-knotted state.  It is possible that a dimerization event proceeds via this jammed state, given its kinetic accessibility. From experimental data \cite{Mallam06a} it is apparent that the YibK dimer folds via two parallel pathways favouring monomeric intermediates that retain significant native-like structure. In the following section, we discuss distinct routes found on the transition network described in Figure \ref{fig:HI0766} that lead to a deeply knotted topology.

\subsubsection*{Pathways to a deep trefoil knot: YibK}

Taylor's theory of knot formation \cite{Taylor00a,Taylor07a} suggests that a protein first assumes the form of a ``twisted hairpin'' followed by the threading of one terminus through the ``eye'' of the hairpin to create a trefoil knot. The model provides an elegant solution, as few moves are required to achieve a complex topology. However, the motion of twisting a loop one or more times comes at a heavy thermodynamic cost. The mechanism is perhaps feasible for a trefoil knot where only one twist is required before threading but becomes less likely for the more complex knots, as multiple twists are needed.  In Figure \ref{fig:kdpaths_1mxi20} we illustrate the three most kinetically relevant pathways found for converting the jammed slip-knot intermediate to the native deep trefoil knot.

The path where the C-terminus threads through a pre-formed loop is depicted in blue in Figure \ref{fig:kdpaths_1mxi20}, and as P3 in Figure \ref{fig:kdpathsfig_1mxi20}. Among the three physically distinct mechanisms identified, coloured in red, green and blue in Figure \ref{fig:kdpaths_1mxi20}, P3 is the least thermodynamically favourable path.  Here, the C-terminus threads through a pre-ordered loop formed by $\beta$1-L4-$\alpha$3-L5.  The path is similar to the \textit{knot-late} pathway suggested in reference \cite{Wallin07a}, where additional specific non-native interactions were added to increase success rate of folding with molecular dynamics.  The set of paths coloured in green correspond to the C-terminal forming a slip-knot while passing through the same pre-ordered loop. This loop is where the knot nucleates for both pathways P2 and P3. 

A significant thermodynamic advantage is gained by a combination of loop-flipping\cite{Flapan19a} and slip-knotting at the N-terminus depicted as pathway P1 in red in Figure \ref{fig:kdpaths_1mxi20}. The shortest path corresponds to a mechanism where a flexible  motif ($\beta$2-L6-$\alpha$4) expands to accommodate the long tail formed at the N-terminal. The long tail then forms a slip-knot, as the extended loop passes through the slip-knot, resulting in the final native knotted state. The energy cost for extending the loop is of the order of 45$\varepsilon$ and once the N-terminus passes through the extended loop, little rearrangement is required to reach the native state. Most native contacts are almost spontaneously formed. The fastest path we find to the deep knot from a slip-knotted state suggests that there is an inherent thermodynamic advantage conferred by the mobility of a loop.  A loop-flipping mechanism was observed as a possible pathway for unfolding a Stevedore (6$_1$) knot \cite{Bolinger10a}. Pathways obtained from our simulations suggest that loop extension followed by a combination of loop-flipping and slip-knotting through the extended loop is the most thermodynamically favourable path to a deep trefoil knot from a jammed slip-knot. The preferred order (in terms of length of the path) flips when starting from a high-energy local minimum. An example is discussed in supplementary information. We predict that once a jammed slip-knot structure is attained, the loop-flipping-plus-slip-knotting mechanism becomes more favoured than threading a terminus through a pre-formed loop\cite{Taylor07a} or loop-flipping alone\cite{Flapan19a}. 

Further, an even longer N-terminal could  follow the same path as P1 as slip-knotting through an extended loop has a  lower thermodynamic cost than slip-knotting through a narrow loop formed closer to the C-terminal. For the entire 168 residue protein, the knotted region is 40 residues from the C-terminal and 70 residues from the N-terminal. The mechanism suggested in path P1 is a likely route for the longer N-terminal to thread the loop first via a slip-knot. The mechanism is qualitatively similar to the \textit{mouse-trap} mechanism\cite{Covino14a} but along with a concerted motion of a flexible loop around the N-terminal, it includes a slip-knotting motion as well as subsequent rearrangements to reach the native-state. To our knowledge, this is the first report of a \textit{mouse-trap}-like path for a native-only coarse-grained model. In keeping with previous studies with the native-only C$\alpha$ model, no such path was found for MJ0366. It is likely that this mechanism is favoured for longer proteins with deep trefoil knots. Mechanical tightening of a natively slip-knotted protein AFV3-109 with single-molecule force spectroscopy revealed parallel pathways for converting the native slip-knotted structure into a trefoil knotted one\cite{He14a,He12a}. The pathways suggested above are also plausible paths for converting a jammed slip-knotted structure to a deep-knotted one. 

\subsection*{Energy landscape of a protein with a Gordian knot: UCH-L1}


Ubiquitin C-terminal hydrolase isoenzyme L1 (UCH-L1) is a deubiquitinating enzyme that cleaves ubiquitin at its carboxyl terminal. It is linked to neurodegenerative diseases such as Parkinson’s\cite{Marganore04a} and Alzheimer’s\cite{Zhangm14a}. It has six $\beta$-strands that form a $\beta$-sheet surrounded by 10 $\alpha$-helices. The tail on the N-terminus has 5 residues and on the C-terminus has 10 residues while the remaining 216 residues form the knotted core resulting in a shallow Gordian knot.

In the disconnectivity graph for this protein (Figure \ref{fig:UCHL1}),  9.6\% of the minima were classified as trefoil knots while 0.3\% were classified as Figure-of-eight knots and the rest as unknots. The sub-basin D2 in green comprises only trefoil knots indicating the presence of a topological trap in a jammed trefoil-knotted state. 
%A three-dimensional metric disconnectivity graph \cite{Smeeton15a} is also plotted in Figure \ref{fig:UCHL1}b with energy on the z-axis, root mean square deviation from the native-state on the x-axis and knot-type on the y-axis.
Three clear topological traps emerge as sub-basins on the disconnectivity graph. The sub-basin D1 belongs to a structure where the N-terminus exits the flexible loop formed by $\alpha$6 and $\alpha$7. This motion results in complete unknotting of the Gordian knot in one simple step. A barrier of 10$\varepsilon$ is required to backtrack to the native Gordian knot. The second sub-basin, D2, comprises structures that only have a trefoil knot. Here, the longer C-terminus unties first while the C-terminus remains threaded resulting in structures classified as trefoil knots. A topological barrier of 30$\varepsilon$ needs to be overcome to backtrack from the trefoil knotted sub-basin to the native Gordian knot. Thus backtracking can also proceed from a jammed trefoil knot to a Gordian knot.  The largest sub-basin, D3, corresponds to an unknotted structure where both the N and C termini have exited their respective loops but form native like contacts without threading the loop. Again, a large 30$\varepsilon$ is needed to backtrack and correctly form a Gordian knotted structure. 
\subsubsection*{Pathways to a Gordian knot}
Pathways starting from 500 high-energy local minima to the native Gordian knotted state are showed in Figure \ref{fig:2len_paths}. Unknotting from the N-terminus, which comprises a smaller tail of five residues is the most favoured mechanism. The tail is surrounded by a large flexible loop (residues 146-159 connecting $\alpha$7 and $\beta$3). The smaller tail exits this loop resulting in a structure classified as an unknot. The $\alpha$7-$\alpha$8-$\beta$3 motif is quite flexible and opens up first resulting in unfolding. It is also possible for the C-terminus to untie while the N-terminus remains inside the loop. This mechanism results in a structure that is classified as a trefoil knot. The tail at the C-terminus has 10 residues. Usually, this tail exits the loop formed by $\alpha$1-$\beta$1 motif resulting in a trefoil knot.    Interestingly, we also found very few structures classified as figure-of-eight knots on the landscape. This occurs when both the N and C-termini untie and then the C-terminus enters the rather large loop previously occupied the C-terminus. Thus, the protein unknots, forms a figure-of-eight knot, unknots again and then forms a Gordian knot. 

\section{Conclusions}
%figures
%2efv
\newpage
\begin{figure} [htbp]
    \centering
    \includegraphics{figures/MJ0366.pdf}
    \caption{Disconnectivity graph for MJ0366 comprising 20126 minima and 30171 transition states. Green nodes represent local minima in a 3$_1$ knotted state, while black nodes depict local minima in an unknotted state. A1 and A2 correspond to the local minima at the bottom of the next biggest kinetic traps. Native-like knots begin to appear around 42$\varepsilon$ above the native state.\label{fig:MJ0366}}
\end{figure}
\begin{figure}
    \centering
    \includegraphics{figures/2efv_paths.pdf}
    \caption{552 distinct paths belonging to three physically distinct pathways connecting the topological trap A2 to the native knotted state N extracted from the disconnectivity graph. The path P1 (in red) shows that the  N-terminus enters the pre-ordered native-like scaffold in a slip-knotted fashion. This is the shortest, most preferred pathway. The pathway P2 (in green) includes opening up the pre-ordered ($\alpha$1-$\alpha$2) loop to let the C-terminal ($\alpha$4-helix) out of the loop, followed by entry of the N-terminus through a slipknot and re-entry of the C-terminus resulting in a shoe-lace like intermediate. Finally, P3 (in blue) shows the entry of the N-terminus, followed by formation of the native-like scaffold,  followed by entry of the C-terminus. A video depicting one representative path from the sets P1, P2 and P3 is provided in the Supplementary Information. Similarly, 1152 distinct paths were found connecting the sub-basin A1 to N with qualitatively the same physically distinct pathways. \label{fig:kdpaths_2efv_fig}. \label{fig:kdpaths_2efv} }
\end{figure}
%1mxi:
\newpage
\begin{figure}
    \centering
    \includegraphics[\textwidth]{figures/1mxi20_tree.pdf}
    \caption{ Left: Disconnectivity graph derived for YibK truncated at 20 residues on either side of the knotted region (93 residues in total) comprising 57639 minima and 86307 transition states. The jammed slip-knot structure appears as a major kinetic trap. Right: YibK truncated at 5 residues on either side of the knotted region (53 residues) comprising 27068 minima and 44625 transition states connected to each other. Green nodes represent local minima in a 3$_1$ knotted state while black nodes depict local minima in an unknotted state. Minima classified as trefoil knots are coloured green while unknots are coloured black. }
    \label{fig:HI0766}
\end{figure}
\begin{figure}[htbp]
    \centering
    \includegraphics{figures/1mxi_paths.pdf}
    \caption{48 distinct paths found connecting the largest topological trap (a jammed slip-knot) to the native trefoil knotted state N. Colours indicate the three mechanically distinct pathways found. Paths in red (set P1) correspond to the loop-flipping plus slip-knotting mechanism from the N-terminal, pathways in green correspond to the slip-knotting mechanism from the C-terminal and pathways in blue correspond to the threading mechanism from the C-terminus. A video depicting one representative path from each set is provided in Supplementary Information. }
    \label{fig:kdpaths_1mxi20}
\end{figure}
%2len
\begin{figure}
    \centering
    \includegraphics{figures/UCHL1.pdf}
    \caption{ Disconnectivity graph for UCH-L1 comprising 11126 minima and 15582 transition states. Gordian (orange nodes), figure-of-eight (blue nodes) and trefoil (green nodes) type knots were found on the landscape. Three clear sub-basins, D1, D2 and D3 emerge. D2 corresponds to a jammed trefoil knot while D1, and D3 sub-basins remain unknotted.     \label{fig:UCHL1}
 }
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/2len_paths.pdf}
    \caption{Folding pathways from 1000 highest energy local minima found on the landscape to the native Gordian knotted state. Knot-type for every stationary point on each pathway was found and classified as unknot (Un), trefoil(Tr), figure-of-eight(fo8) or Gordian(Go). x-axis is the number of stationary points visited, y-axis is relative energy ($\varepsilon$) and the z-axis is the knot type of each stationary point. }
    \label{fig:2len_paths}
\end{figure}

%\subsection*{Manuscript Length}
%PNAS generally uses a two-column format averaging 67 characters, including spaces, per line. The maximum length of a Direct Submission research article is six pages and a Direct Submission Plus research article is ten pages including all text, spaces, and the number of characters displaced by figures, tables, and equations.  When submitting tables, figures, and/or equations in addition to text, keep the text for your manuscript under 39,000 characters (including spaces) for Direct Submissions and 72,000 characters (including spaces) for Direct Submission Plus.


% \begin{figure}%[tbhp]
% \centering
% \includegraphics[width=.8\linewidth]{frog}
% \caption{Placeholder image of a frog with a long example caption to show justification setting.}
% \label{fig:frog}
% \end{figure}



%\showmatmethods{} % Display the Materials and Methods section

%\acknow{S.N. acknowledges support from the National Centre for Biological Sciences through the NCBS/InSTEM-Cambridge (NiC) fellowship. S.N. and S.G. acknowledge support from core funding from the Tata Institute of Fundamental Research (TIFR). D.J.W. acknowledges support from the EPSRC.}

%\showacknow{} % Display the acknowledgments section

% Bibliography
\clearpage
\bibliography{pnas-sample}

\end{document}